package com.davidgassner.plainolnotes;

import java.util.List;

import com.davidgassner.plainolnotes.data.NoteItem;
import com.davidgassner.plainolnotes.data.NotesDataSource;

import android.os.Bundle;
//import android.provider.ContactsContract.Contacts.Data;
//import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.view.ContextMenu;
//import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

//public class MainActivity extends Activity {
// to display data as lists instead of activity extend list activity
public class MainActivity extends ListActivity {
	private static final int EDITOR_ACTIVITY_REQUEST = 1001;
	private static final int MENU_DELETE_ID = 1002;
	private int currentNoteId;
	// data source instance
	private NotesDataSource dataSource;
	List<NoteItem> notesList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		registerForContextMenu(getListView());
		dataSource = new NotesDataSource(this);

		refreshDisplay();

		/*
		 * List<NoteItem> notes = dataSource.findAll(); NoteItem note =
		 * notes.get(0); note.setText("Updated!");
		 * 
		 * //put data in shared resources object dataSource.update(note); notes
		 * = dataSource.findAll(); note = notes.get(0);
		 * 
		 * Log.i("message_rakesh, key: ",note.getKey()+" value : "+note.getText()
		 * );
		 */
	}

	private void refreshDisplay() {
		// retrieve data from persistent storage
		notesList = dataSource.findAll();
		ArrayAdapter<NoteItem> adapter = new ArrayAdapter<NoteItem>(this,
				R.layout.list_item_layout, notesList);
		setListAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.action_create) {
			createNote();
		}
		return super.onOptionsItemSelected(item);
	}

	private void createNote() {
		NoteItem noteItem = NoteItem.getNew();
		Intent intent = new Intent(this, NoteEditorActivity.class);
		intent.putExtra("key", noteItem.getKey());
		intent.putExtra("text", noteItem.getText());
		startActivityForResult(intent, EDITOR_ACTIVITY_REQUEST);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		NoteItem noteItem = notesList.get(position);
		Intent intent = new Intent(this, NoteEditorActivity.class);
		intent.putExtra("key", noteItem.getKey());
		intent.putExtra("text", noteItem.getText());
		startActivityForResult(intent, EDITOR_ACTIVITY_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == EDITOR_ACTIVITY_REQUEST && resultCode == RESULT_OK) {
			NoteItem noteItem = new NoteItem();
			noteItem.setKey(data.getStringExtra("key"));
			noteItem.setText(data.getStringExtra("text"));
			dataSource.update(noteItem);
			refreshDisplay();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		currentNoteId = (int)info.id;
		//0 - group id. Other value if groups of menu items AND our id is now assigned to MENU_DELETE_ID
		menu.add(0,MENU_DELETE_ID,0,"Delete?");
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if(item.getItemId() == MENU_DELETE_ID){
			NoteItem noteItem = notesList.get(currentNoteId);
			dataSource.remove(noteItem);
			refreshDisplay();
		}
		
		return super.onContextItemSelected(item);
	}
}
