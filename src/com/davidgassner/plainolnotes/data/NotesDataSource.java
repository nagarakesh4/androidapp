package com.davidgassner.plainolnotes.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import android.content.Context;
import android.content.SharedPreferences;

public class NotesDataSource {

	private static final String PREFKEY = "notes";
	private SharedPreferences notePrefs;

	public NotesDataSource(Context context) {
		notePrefs = context.getSharedPreferences(PREFKEY, Context.MODE_PRIVATE);
	}

	// return list of notes
	public List<NoteItem> findAll() {

		// retrieve data from sharedpref obj
		// retrieves from map
		Map<String, ?> notesMap = notePrefs.getAll();
		
		// the above data is unordered so sort the data
		// value is got from treeset. in const of treeset keyset() of map is
		// sent. tree set returns all the keys for all the notes in the
		// sharedpreferences in any order, the tree set returns in sorted set. oldest->newest
		SortedSet<String> keys = new TreeSet<String>(notesMap.keySet());

		List<NoteItem> noteList = new ArrayList<NoteItem>();
		for(String key : keys){
			NoteItem note = new NoteItem();
			note.setKey(key);
			note.setText((String) notesMap.get(key));
			noteList.add(note);
		}
		
		/*NoteItem noteItem = NoteItem.getNew();
		noteList.add(noteItem);*/
		
		return noteList;
	}

	// add or update note
	public boolean update(NoteItem note) {
		SharedPreferences.Editor editor = notePrefs.edit();
		editor.putString(note.getKey(), note.getText());
		editor.commit();
		return true;
	}

	// remove note
	public boolean remove(NoteItem note) {
		if (notePrefs.contains(note.getKey())) {
			SharedPreferences.Editor editor = notePrefs.edit();
			editor.remove(note.getKey());
			editor.commit();
		}
		return true;
	}
}
